package MarireVladFlorina;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;


public class Sub2 extends JFrame {
    public JFrame frame;

    public Sub2() {

        frame = new JFrame("Exam");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800, 500);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        panel.setLayout(null);

        JTextField textf1 = new JTextField();
        textf1.setBounds(200, 40, 200, 30);
        panel.add(textf1);

        JTextField textf2 = new JTextField();
        textf2.setBounds(200, 100, 200, 30);
        panel.add(textf2);


        JButton b = new JButton("calculate");
        b.setBounds(200, 250, 200, 50);
        panel.add(b);

        JLabel label1 = new JLabel();
        JLabel label2 = new JLabel();

        label1.setText("Fisier1");
        label2.setText("Fisier2");

        label1.setBounds(30, 40, 150, 20);
        label2.setBounds(30, 100, 150, 20);

        panel.add(label1);
        panel.add(label2);

        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e1) {

                String filename1 = textf1.getText();
                String filename2 = textf2.getText();

                try (FileWriter fileWriter = new FileWriter("fisier1" + ".txt")) {
                    fileWriter.write(filename1);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try (FileWriter fileWriter = new FileWriter("fisier2" + ".txt")) {
                    fileWriter.write(filename2);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        });


        frame.add(panel);
        frame.setContentPane(panel);
        frame.setVisible(true);
    }

    public static void main(String[] args) {

        new Sub2();
    }
}

class Reader implements Runnable {
    File source;
    File destination;

    public Reader(File source, File destination) {
        this.source = source;
        this.destination = destination;
    }

    @Override
    public void run() {
        String content;
        content = readFromFile(source.getAbsolutePath());
        writeToFile(destination, content);
    }

    private static void writeToFile(File file, String content) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
            writer.write(content);
            writer.write("file content written");
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    static String readFromFile(String filename) {
        StringBuffer content = new StringBuffer();
        try {
            String text;
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            while ((text = reader.readLine()) != null) {
                content.append(text);
                content.append("\n");

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content.toString();
    }

}

