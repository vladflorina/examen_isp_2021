import java.util.ArrayList;

public class Subiectul1 {

    public class U {
        public void p() {

        }

    }

    public class I extends U {
        private long t;
        private K k;

        public void f() {

        }

    }

    public class J {

        public void i(I I) {

        }
    }

    public class K {

        private ArrayList<M> arrayListM;
        private L[] arrayL;


        public K() {
            this.arrayListM = new ArrayList<M>();
            this.arrayL = new L[100];
        }
    }


    public class L {

        public void metA() {

        }
    }

    public class N {
        private I i;
    }

    public class M {
        public void metB() {

        }
    }

}
