import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;

public class Subiectul2 {



    public static void doTheJob(JTextField f1, JButton b1, JTextField j2){



            b1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    String filename = f1.getText();
                    try{
                        File file = new File(filename);
                        FileInputStream fileInputStream = new FileInputStream(file);
                        int countChars = 0;

                        while(fileInputStream.read() != -1){
                            countChars++;
                        }

                        j2.setText(countChars + "");
                    }
                    catch(Exception ex){
                        ex.printStackTrace();
                    }
                }
            });

    }

    public static void main(String[] args) {

        JFrame frame = new JFrame("Subiectul2");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600, 200);

        JPanel panel1 = new JPanel();
        JTextField textField1 = new JTextField();
        JButton button1 = new JButton("Buton");
        JTextField textField2 = new JTextField();

        textField1.setBounds(20, 50, 100, 20);
        button1.setBounds(20, 170, 100, 40);
        textField2.setBounds(50, 50, 80, 20);

        panel1.add(textField1);
        panel1.add(button1);
        panel1.add(textField2);



        frame.add(panel1);

        doTheJob(textField1, button1, textField2);

        frame.setContentPane(panel1);
        frame.setVisible(true);
    }

}
